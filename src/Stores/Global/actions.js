export default {
    UPDATE_STATE: function (payload) {
        return {
            type: 'UPDATE_STATE',
            payload
        }
    }
}