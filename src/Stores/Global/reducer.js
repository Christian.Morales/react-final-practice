export let initialState = {
    user: "",
    questions: [
        { id: 'question1', text: 'question text 1', options: [{ id: 'a', text: 'a', selected: true }] },
        { id: 'question2', text: 'question text 2', options: [{ id: 'a', text: 'a', selected: true }, { id: 'b', text: 'b', selected: false }, { id: 'c', text: 'c', selected: false }] },
        { id: 'question3', text: 'question text 3', options: [{ id: 'a', text: 'a', selected: true }, { id: 'b', text: 'b', selected: false }, { id: 'c', text: 'c', selected: false }] },
        { id: 'question4', text: 'question text 4', options: [{ id: 'a', text: 'a', selected: true }, { id: 'b', text: 'b', selected: false }, { id: 'c', text: 'c', selected: false }] },
        { id: 'question5', text: 'question text 5', options: [{ id: 'a', text: 'a', selected: true }, { id: 'b', text: 'b', selected: false }, { id: 'c', text: 'c', selected: false }] }
    ],
    reorderQuestions: true,
    reorderOptions: true
}
function updateState(state, action) {
    return { ...state, ...action.payload }
}
export default function globalReducer(state, action) {
    switch (action.type) {
        case "UPDATE_STATE":
            return updateState(state, action)
        default:
            return state
    }
}