import React from 'react';
import User from './Containers/UserForm'
import Test from './Containers/Test'
import Results from './Containers/Results'
import "materialize-css/dist/css/materialize.css";
import "materialize-css"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <User />
        </Route>
        <Route path="/test">
          <Test />
        </Route>
        <Route path="/results">
          <Results />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
