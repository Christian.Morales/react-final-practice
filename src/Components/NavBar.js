import React from "react";
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
function NavBar() {
    const user = useSelector(state => state.user)
    return (
        <div className="navbar-fixed ">
            <nav className="blue">
                <div className="nav-wrapper container">
                    <Link to="/" className="brand-logo">Frontend Practice</Link>
                    <ul className="right hide-on-med-and-down">
                        <li><a href="#!">{user}</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    )
}
export default NavBar