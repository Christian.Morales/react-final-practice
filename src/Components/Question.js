import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import actions from "../Stores/Global/actions";
import { shuffleArray } from "../helper/randomize";
function Question(props) {
    const questions = useSelector(state => state.questions)
    const dispatch = useDispatch()
    let reorder = useSelector(state => state.reorderOptions)
    let { id, text, options } = props.ques



    useEffect(() => {
        if (reorder) {
            shuffleArray(options)
            dispatch(actions.UPDATE_STATE({ reorderOptions: false }))
        }
        return () => { dispatch(actions.UPDATE_STATE({ reorderOptions: true })) }
    }, [])


    function changeStore(idOption) {
        const newQuestions = [...questions];
        let q = newQuestions.find((el) => el.id === id)
        let index = newQuestions.indexOf(q);
        let op = q.options.find((el) => el.id === idOption)
        op.selected = !op.selected
        newQuestions[index] = q
        dispatch(actions.UPDATE_STATE({ questions: newQuestions }))


    }
    return (
        <div className="card" style={{ padding: '10px' }}>
            <h4>{text}</h4>
            {options.map(option => {
                return (
                    <p key={option.id}>
                        <label>
                            <input type="checkbox" checked={option.selected} onChange={() => changeStore(option.id)} />
                            <span>{option.text}</span>
                        </label>
                    </p>
                )
            })}
        </div>
    )
}
export default React.memo(Question)