import React, { useEffect } from "react";
import Question from "./Question";
import { useSelector, useDispatch } from "react-redux";
import { shuffleArray } from "../helper/randomize";
import actions from "../Stores/Global/actions";


function QuestionList() {
    let questions = useSelector(state => state.questions)
    let reorder = useSelector(state => state.reorderQuestions)
    const dispatch = useDispatch()

    useEffect(() => {
        if (reorder) {
            shuffleArray(questions)
            dispatch(actions.UPDATE_STATE({ reorderQuestions: false }))
        }
        return () => { dispatch(actions.UPDATE_STATE({ reorderQuestions: true })) }
    }, [])



    return (
        <div className="container">
            <h4 className="">Questions:</h4>
            {
                questions.map((question) => {
                    return (
                        <Question ques={question} key={question.id} />
                    )
                })
            }
            
        </div>
    )
}
export default React.memo(QuestionList)