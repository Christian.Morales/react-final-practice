import React from "react";
import NavBar from "../../Components/NavBar";
import QList from "../../Components/QuestionList";
import { Link } from "react-router-dom";
import "./index.css";
function Test() {
    return (
        <div className="test">
            <NavBar />
            <QList />
            <Link to="/results" className="btn blue darken-3 btnResult">Finish Test</Link>
        </div>
    )
}
export default Test;