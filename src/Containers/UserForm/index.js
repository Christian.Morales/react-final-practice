import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import actions from '../../Stores/Global/actions'
import { useHistory } from "react-router-dom";
function Home() {
    let history = useHistory();
    const [userState, setUser] = useState('');
    const user = useSelector(state => state.user)
    const dispatch = useDispatch()
    function changeValue(e) {
        setUser(e.target.value)
    }
    function startTest(e) {
        e.preventDefault();
        if (userState !== '') {
            dispatch(actions.UPDATE_STATE({ user: userState }));
            history.push('/test')
        }else{
            alert('Enter your name please')
        }
       
    }

    return (
        <div className="container" >
            <div className="row">
                <form className="col s12" onSubmit={startTest}>
                    <h4 className="center-align">Frontend Practice</h4>
                    <h5 className="center-align">Current Name: {user}</h5>
                    <div className="input-field col s12">
                        <input id="first_name" type="text" value={userState} onChange={changeValue} />
                        <label htmlFor="first_name">Full Name</label>
                    </div>
                    <div className="input-field col s12">
                        <button className="btn blue" style={{ width: '100%' }} > Start Test</button>
                    </div>
                </form>
            </div>

        </div>
    )
}


export default Home;