import React from "react";
import NavBar from "../../Components/NavBar";
import { useSelector } from "react-redux";
function Results() {
    let questions = useSelector(state => state.questions)
    return (
        <div className="resultados">
            <NavBar />
            {
                <code>{JSON.stringify(questions)}</code>
            }
        </div>
    )
}
export default Results